import trio
from weather_app.main import WeatherFetcher


def test_weather_fetcher_fetch_weather():
    weather_fetcher = WeatherFetcher()

    async def _fetch_weather():
        result = await weather_fetcher.fetch_weather(
            "Москва",
            "0c5434a2-6326-41a0-829e-3730ad82ba0e0c5434a2-6326-41a0-829e-3730ad82ba0e")
        assert "Москва" in result

    trio.run(_fetch_weather)


def test_weather_fetcher_start_fetch_weather(qtbot):
    weather_fetcher = WeatherFetcher()
    with qtbot.waitSignal(weather_fetcher.weather_data_fetched, timeout=10000) as blocker:
        weather_fetcher.start_fetch_weather(
            "Москва",
            "0c5434a2-6326-41a0-829e-3730ad82ba0e0c5434a2-6326-41a0-829e-3730ad82ba0e")

    result = blocker.args[0]
    assert "London" in result
