# Weather App

Простое приложение для просмотра погоды, разработанное с использованием Python, PySide6 и Weather API.

## Установка

1. Убедитесь, что у вас установлен Python 3.7 или выше и Poetry.
2. Клонируйте этот репозиторий:
    ```
    git clone https://gitlab.com/Deniszharkovgit/weather-app.git
    ```
3. Перейдите в каталог проекта и установите зависимости:
    ```
    cd weather_app
    poetry install
    ```

Если столкнулись ошибкой `CERTIFICATE_VERIFY_FAILED`), выполните следующие действия для установки корневых сертификатов:

1. Убедитесь, что у вас установлен пакет `ca-certificates`. Если нет, установите его с помощью своего пакетного менеджера.

    Например, для Ubuntu или Debian используйте следующую команду:

    ```
    sudo apt-get install ca-certificates
    ```

    Для Fedora:

    ```
    sudo dnf install ca-certificates
    ```

    Для CentOS:

    ```
    sudo yum install ca-certificates
    ```

2. Обновите список корневых сертификатов, выполнив следующую команду:

    ```
    sudo update-ca-certificates
    ```

После выполнения этих действий проблема с SSL-сертификатами должна быть решена.


## Запуск приложения

1. Активируйте виртуальное окружение Poetry:
    ```
    poetry shell
    ```

2. Запустите приложение:
    ```
    python weather_app/main.py
    ```

## Функции

1. Получение текущей погоды для указанного города.
2. Асинхронное выполнение сетевых запросов для получения данных о погоде, без блокирования пользовательского интерфейса.

## Используемые API

1. Yandex Weather API: https://yandex.ru/dev/weather/doc/dg/concepts/forecast-test.html
