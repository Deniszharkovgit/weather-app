import sys
from PySide6.QtCore import QObject, Signal, QRunnable, QThreadPool, QSettings
from PySide6.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QLineEdit, QPushButton, QLabel, QWidget, \
    QTextEdit
import httpx
import trio
from geopy.geocoders import Nominatim


# Класс WeatherFetcher отвечает за получение данных о погоде из Yandex Weather API
class WeatherFetcher(QObject):
    # Сигнал для отправки полученных данных о погоде
    weather_data_fetched = Signal(str)

    # Асинхронная функция для получения данных о погоде из Yandex Weather API
    async def fetch_weather(self, location_name, api_key):
        # Инициализация геокодера Nominatim
        geolocator = Nominatim(user_agent="weather_app")
        # Получение координат из названия местоположения
        location = geolocator.geocode(location_name)

        # Если местоположение не найдено, вернуть сообщение об ошибке
        if location is None:
            return "Локация не найдена."

        # Формирование URL для запроса данных о погоде
        url = f"https://api.weather.yandex.ru/v2/forecast?lat={location.latitude}&lon={location.longitude}&lang=en_US"
        headers = {"X-Yandex-API-Key": api_key}
        # Отправка запроса к Yandex Weather API
        async with httpx.AsyncClient() as client:
            response = await client.get(url, headers=headers)

        # Если код ответа не равен 200, вернуть сообщение об ошибке
        if response.status_code != 200:
            return f"Ошибка при получении сведений о погоде: {response.status_code}"

        # Извлечение данных о погоде из ответа
        data = response.json()
        temp = data["fact"]["temp"]
        condition = data["fact"]["condition"]
        # Возвращение полученных данных о погоде
        return f"Погода в {location_name}: {temp}°C, {condition}"

    # Функция для начала асинхронного получения данных о погоде
    def start_fetch_weather(self, location_name, api_key):
        # Вложенный класс WeatherRunnable для QThreadPool
        class WeatherRunnable(QRunnable):
            def __init__(self, fetcher, location_name, api_key):
                super().__init__()
                self.fetcher = fetcher
                self.location_name = location_name
                self.api_key = api_key

            # Выполнение асинхронного запроса к Yandex Weather API
            def run(self):
                result = trio.run(self.fetcher.fetch_weather, self.location_name, self.api_key)
                self.fetcher.weather_data_fetched.emit(result)

        # Запуск WeatherRunnable в глобальном пуле потоков
        QThreadPool.globalInstance().start(WeatherRunnable(self, location_name, api_key))


# Основной класс окна приложения
class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # Создание экземпляра класса WeatherFetcher
        self.weather_fetcher = WeatherFetcher()
        # Подключение сигнала с полученными данными о погоде к слоту обработки
        self.weather_fetcher.weather_data_fetched.connect(self.on_weather_data)
        # Создание графических элементов окна приложения
        central_widget = QWidget()
        layout = QVBoxLayout(central_widget)

        self.location_input = QLineEdit()
        self.api_key_input = QLineEdit()
        self.fetch_weather_button = QPushButton("Получение погоды")
        self.weather_info = QTextEdit()

        layout.addWidget(QLabel("Локация:"))
        layout.addWidget(self.location_input)
        layout.addWidget(QLabel("API Ключ:"))
        layout.addWidget(self.api_key_input)
        layout.addWidget(self.fetch_weather_button)
        layout.addWidget(self.weather_info)

        self.setCentralWidget(central_widget)
        self.location_input.textChanged.connect(self.check_input_fields)
        self.api_key_input.textChanged.connect(self.check_input_fields)
        self.fetch_weather_button.clicked.connect(self.fetch_weather)

        # Подгрузка ранее использованного API ключа
        self.settings = QSettings("your-organization-name", "weather-app")
        self.api_key_input.setText(self.settings.value("api_key", ""))

    def check_input_fields(self):
        # Проверяем, что оба поля ввода не пусты
        if self.location_input.text() and self.api_key_input.text():
            self.fetch_weather_button.setEnabled(True)
        else:
            self.fetch_weather_button.setEnabled(False)

    # Слот обработки изменения текста в полях ввода
    def fetch_weather(self):
        location_name = self.location_input.text()
        api_key = self.api_key_input.text()
        # Сохранение API ключа
        self.settings.setValue("api_key", api_key)

        self.weather_fetcher.start_fetch_weather(location_name, api_key)

    # Слот обработки полученных данных о погоде
    def on_weather_data(self, data):
        self.weather_info.setText(data)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()

    sys.exit(app.exec())
